//Author Thomas Back, Javascript code for clock game
var gameport = document.getElementById("gameport");

var renderer = PIXI.autoDetectRenderer(400, 400, {backgroundColor: 0x00BFFF});
gameport.appendChild(renderer.view);

var stage = new PIXI.Container();


var backgroundImage = PIXI.Sprite.fromImage("stage.png");
stage.addChild(backgroundImage);
//text for game over menu
var gameover = new PIXI.Text("Game over my friend.",{font : '24px Arial', fill : 0xff1010, align : 'center'});
//create score counter
var score = new PIXI.Text('0',{font : '24px Arial', fill : 0xff1010, align : 'center'});
//create sprites
var cog = new PIXI.Sprite(PIXI.Texture.fromImage("Cog1.png"));
var clockarrow = new PIXI.Sprite(PIXI.Texture.fromImage("clockarrow.png"));
var player = new PIXI.Sprite(PIXI.Texture.fromImage("player.png"));
var dustbunnytexture = new PIXI.Texture.fromImage('dustbunny.png');
var ingame = true;
//addscoreboard to world
stage.addChild(score);
score.position.x = 350;
score.position.y = 0;
var scorecount = 0;



//add the clock arrow
stage.addChild(clockarrow);
clockarrow.anchor.x = 0.5;
clockarrow.anchor.y = 0.9;
clockarrow.position.x = 200;
clockarrow.position.y = 200;

//add player
stage.addChild(player);
player.anchor.x = 0.5;
player.anchor.y = 0.5;
player.position.x = 100;
player.position.y = 100;

//add the cog
stage.addChild(cog);
cog.position.x = Math.floor(Math.random() * 300) + 50;
cog.position.y = Math.floor(Math.random() * 300) + 50;
cog.anchor.x = 0.5;
cog.anchor.y = 0.5;

//add multiple dustbunnies
var dbarr = [];
for(var i = 0; i < 4; i++){
    var db = new PIXI.Sprite(dustbunnytexture);
    db.anchor.x = 0.5;
    db.anchor.y = 0.5;
    db.position.x = Math.floor(Math.random() * 300) + 50;
    db.position.y = Math.floor(Math.random() * 300) + 50;
    //prevent from spawning at same location as player
    while(collision(player, db)){
        db.position.x = Math.floor(Math.random() * 300) + 50;
        db.position.y = Math.floor(Math.random() * 300) + 50;
    }
    while(collision(cog,db )){
        db.position.x = Math.floor(Math.random() * 300) + 50;
        db.position.y = Math.floor(Math.random() * 300) + 50;
    }
    db.anchor.x = 0.5;
    db.anchor.y = 0.5;
    dbarr[i] = db;
    stage.addChild(db);
}
//this function is called to reinitialize the game once ended
function init(){
    score.setText("0");
    gameover.alpha = 0;
    ingame = true;
    player.rotation = 0;
    for(var i = 0; i < 4; i++){
   

        dbarr[i].position.x = Math.floor(Math.random() * 300) + 50;
        dbarr[i].position.y = Math.floor(Math.random() * 300) + 50;
        //prevent from spawning at same location as player
        while(collision(player, dbarr[i])){
            dbarr[i].position.x = Math.floor(Math.random() * 300) + 50;
            dbarr[i].position.y = Math.floor(Math.random() * 300) + 50;
        }
    dbarr[i].anchor.x = 0.5;
    dbarr[i].anchor.y = 0.5;
}
    cog.position.x = Math.floor(Math.random() * 300) + 50;
    cog.position.y = Math.floor(Math.random() * 300) + 50;
    cog.anchor.x = 0.5;
    cog.anchor.y = 0.5;
    
    
}


//add game over menu
stage.addChild(gameover);
gameover.position.x = 100;
gameover.position.y = 100;
gameover.alpha = 0;

function keydownEventHandler(e){
    if (e.keyCode == 13 && !ingame){
        scorecount = 0;
        init();
    }
    if (e.keyCode ==  87) {// w key
        player.position.y -= 10;
    }
    if (e.keyCode == 83) {
        player.position.y += 10;
    }
    if (e.keyCode == 65){
        player.position.x -= 10;
    }
    if (e.keyCode == 68) {
        player.position.x += 10; //now we need event listener
    }
    if (e.keyCode == 13 && !ingame){
        scorecount = 0;
        init();
    }
    if(collision(player, cog)){
        
       
        cog.position.x = Math.floor(Math.random() * 300) + 50;
        cog.position.y = Math.floor(Math.random() * 300) + 50;
        //loop through db array to make sure cog doesnt spawn where db is
        
        for(i = 0; i < dbarr.length; i++ ){
            while(collision(cog,dbarr[i])){
                cog.position.x = Math.floor(Math.random() * 300) + 50;
                cog.position.y = Math.floor(Math.random() * 300) + 50;
            }
        }
        scorecount += 1;
        score.setText(scorecount);
        
    }
    //loop through dust bunny positions and check if collision occured
    for (i = 0; i < dbarr.length; i++){
        if(collision(player, dbarr[i])){
            player.rotation = 0.5;
            gameover.alpha = 1;
            ingame = false;

        }
    }   
}


document.addEventListener('keydown', keydownEventHandler);

//will make the bunnies move every 10  ms
setInterval(dbmovement, 10);

//function to move bunnies toward player
function dbmovement(){
    for (i = 0; i < dbarr.length;i++){
        if(dbarr[i].position.x <= player.position.x){
            dbarr[i].position.x += 0.1;
        }
        else{
            dbarr[i].position.x -= 0.1;
        }
        if(dbarr[i].position.y <= player.position.y){
            dbarr[i].position.y += 0.1;
        }
        else{
            dbarr[i].position.y -= 0.1;
        }
        if(collision(player,dbarr[i])){
            player.rotation = 0.5;
            gameover.alpha = 1;
            ingame = false;
        }
    }
}
//collision detection algorithm
function collision(ob1, ob2){
    return !(ob2.x > (ob1.x + ob1.width-10) || 

           (ob2.x + ob2.width-10) < ob1.x || 

           ob2.y > (ob1.y + ob1.height-10) ||

           (ob2.y + ob2.height-10) < ob1.y);
}
function animate(){
    requestAnimationFrame(animate);
    clockarrow.rotation += 0.025;
    cog.rotation += 0.05;
    //player.rotation += 0.05;
    
    
    renderer.render(stage);
}

animate();


